const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const User = require("../models/User");

// Register
module.exports.registerUser = (req, res) => {

	User.findOne({email: req.body.email}).then(result => {
		if (result != null && result.email == req.body.email){
			return res.send('Duplicate email found')
		} else {

			const hashedPW = bcrypt.hashSync(req.body.password, 10);

			let newUser = new User ({
				email : req.body.email,
				password : hashedPW
			});

			newUser.save()
			.then(user => res.send(user))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

// Login
module.exports.loginUser = (req, res) => {

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null) {
			return res.send("Incorrect Email");
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if (isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Incorrect Password")
			}
		}
	})
	.catch(err => res.send(err));
};

// Update User To Admin
module.exports.updateToAdmin = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new : true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};

// Show Users
module.exports.showUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}


// Checkout
module.exports.buy = async (req, res) => {

	console.log(req.user.id);
	console.log(req.body.productId);
	console.log(req.body.totalAmount);

	if (req.user.isAdmin) {
		return res.send("Action Forbidden")
	}
	
	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		let newOrder = {
			orderId: req.body.productId,
			totalAmount : req.body.totalAmount

		}

		user.order.push(newOrder);

		return user.save()
		.then(user => true)
		.catch(err => err.message);
	})

	if (isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isProductUpdated = await Product.findById(req.body.productId).then(product => {
		console.log(product);

		let buyer = {
			buyerId: req.user.id,
			totalAmount : req.body.totalAmount
		}

		product.buyer.push(buyer)

		return product.save().then(product => true).catch(err => err.message)
	})

	if (isProductUpdated !== true){
		return res.send({message: isProductUpdated})
	}

	if (isUserUpdated && isProductUpdated) {
		return res.send({message: 'Order Successfully!'})
	}

};


// Retrieve User Order
module.exports.retrieveUserOrders = (req, res) => {
	User.findById(req.user.id)
	.then(result =>{
		res.send(result.order)
	})
	.catch(error => res.send(error))
};

