const mongoose = require ('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	order: [
		{
			orderId: {
				type: String,
				required: [true, "orderId is required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "totalAmount is required"]
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}
	]
	
});

module.exports = mongoose.model("User", userSchema)
