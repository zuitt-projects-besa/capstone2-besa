const express = require('express');
const router = express.Router();
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

// import user controllers
const userControllers = require("../controllers/userControllers");

// Routes
router.post("/registerUser", userControllers.registerUser);

router.post("/loginUser", userControllers.loginUser);

router.put("/updateToAdmin/:id", verify, verifyAdmin, userControllers.updateToAdmin)

router.get("/showUsers", userControllers.showUsers)

router.post("/buy", verify, userControllers.buy)

router.get("/retrieveUserOrders", verify, userControllers.retrieveUserOrders)



module.exports = router;